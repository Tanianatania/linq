﻿using Castle.Core;
using LINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        static HttpClient client = new HttpClient();
        //1+
        static async Task GetCountOfTaskInProjectByUserId(int Id)
        {
            List<Tasks> tasks = await GetAllTask(Route.Tasks);
            List<Project> projects = await GetAllProject(Route.Projects);
            var result = from project in projects
                         join task in tasks on project.id equals task.project_id
                         where task.performer_id == Id
                         group task by project into ress
                         select new { ress.Key, CountOfTask = ress.Count() };
            Console.WriteLine($"Count of task in project:");
            foreach (var item in result)
            {
                Console.WriteLine($"\tName: {item.Key.name} , Count: {item.CountOfTask}");
            }
            Console.WriteLine();
        }
        //7
        static async Task GetByProjectId(int Id)
        {
            List<Tasks> tasks = await GetAllTask(Route.Tasks);
            List<Project> projects = await GetAllProject(Route.Projects);
            List<User> users = await GetAllUser(Route.Users);
            List<Team> teams = await GetAllTeam(Route.Teams);
            var result = from project in projects
                         where project.id == Id
                         select new
                         {
                             project,
                             TaskWithLongestDescrition = tasks.Where(a => a.project_id == Id).OrderByDescending(a => a.description.Length).First(),
                             TaskWithShortestName = tasks.Where(a => a.project_id == Id).OrderBy(a => a.name.Length).First(),
                             CountOfUser =
                             (from pro in projects
                              join task in tasks on pro.id equals task.project_id
                              group task by pro into ress
                              where ress.Count() < 3 || ress.Key.description.Length > 25
                              select new
                              {
                                  project = ress.Key,
                                  CountOfUserInTeam = (from user in users
                                                       where user.team_id == ress.Key.team_id
                                                       select user).Count()
                              })                      
                         };
            foreach (var item in result)
            {
                Console.WriteLine($"Project:\n {item.project}\n" +
                    $" TaskWithLongestDescrition:\n {item.TaskWithLongestDescrition}\n" +
                    $" TaskWithShortestName:\n {item.TaskWithShortestName}\n Count of User:\n");
                foreach(var count in item.CountOfUser)
                {
                    Console.WriteLine($"\tProject name: {count.project.name}  Count of user in team: {count.CountOfUserInTeam}");
                }
            }
        }
        //6+
        static async Task GetByUserId(int Id)
        {
            List<Tasks> tasks = await GetAllTask(Route.Tasks);
            List<Project> projects = await GetAllProject(Route.Projects);
            List<User> users = await GetAllUser(Route.Users);
            List<Team> teams = await GetAllTeam(Route.Teams);
            var result = from user in users
                         where user.id == Id
                         select new
                          {
                             user,
                             MinProject = projects.Where(a => a.author_id == Id).OrderByDescending(a => a.created_at).First(),
                             CountOfTaskInMinProject = tasks.Where(a => a.project_id == (projects.Where(b => b.author_id == Id).OrderByDescending(b => b.created_at).First().id)).Count(),
                             CountOfCanceledOrNotFinishedTasks = tasks.Where(a => a.performer_id == Id && (a.state == 2 || a.state == 4)).Count(),
                             LongestTask = tasks.Where(a => a.performer_id == Id).OrderByDescending(a => (a.finished_at - a.created_at)).First()
                         };

            foreach(var item in result)
            {
                Console.WriteLine(item.user);
                Console.WriteLine("\tMin Project: \n" + item.MinProject);
                Console.WriteLine("\tCount of tasks in Min Project:" + item.CountOfTaskInMinProject);
                Console.WriteLine("\tCount of Canceled or Not finished tasks: "+ item.CountOfCanceledOrNotFinishedTasks);
                Console.WriteLine("\tLongest user`s task: \n" + item.LongestTask);
            }
        }
        //2+
        static async Task GetTaskOfUser(int Id)
        {
            List<Tasks> tasks = await GetAllTask(Route.Tasks);
            var result = tasks.Where(a => a.performer_id == Id && a.name.Length <45);
            Console.WriteLine("Tasks:");
            foreach(var item in result)
            {
                Console.WriteLine("\t" + item);
            }
        }
        //3
        static async Task GetFinishedTasks(int Id)
        {
            List<Project> projects = await GetAllProject(Route.Projects);
            List<Tasks> tasks = await GetAllTask(Route.Tasks);

            var result = tasks.Where(a => a.performer_id == Id && a.finished_at.Year == 2019).Select(a=>(a.id, a.name));

            Console.WriteLine($"Lit of tasks that was finished in 2019 by perfomer with id {Id}");
            foreach (var item in result)
            {
                Console.WriteLine($"\tId: {item.id} , Name: {item.name}");
            }
        }
        //4+
        static async Task GetTeamWithYoungUser()
        {
            List<Team> teams = await GetAllTeam(Route.Teams);
            List<User> users = await GetAllUser(Route.Users);
            var userss = users.Where(a => a.team_id == 100);
            var result = from team in teams
                         join user in users on team.id equals user.team_id
                         orderby user.registered_at
                         group user by team into ress
                         where ress.All(a => DateTime.Now.Year - a.birthday.Year >= 12)
                         select new
                         {
                             Id = ress.Key.id,
                             Name = ress.Key.name,
                             Users = ress
                         };

            Console.WriteLine("List of teams and their participants who are older than 12 years old, sorted by the date of registration of the user in descending order, and grouped by teams.");
            foreach(var item in result)
            {
                Console.WriteLine($"Id: {item.Id}, Name of Team: {item.Name}");
                Console.WriteLine("List of participants:");
                foreach(var user in item.Users)
                {
                    Console.WriteLine("\t"+user);
                }
            }
        }
        //5+
        static async Task GetSortedListByUserAndTeam()
        {
            List<User> users = await GetAllUser(Route.Users);
            List<Tasks> tasks = await GetAllTask(Route.Tasks);
            var result = from user in users
                         join task in tasks on user.id equals task.performer_id
                         orderby user.first_name
                         group task by user into ress
                         select new { ress.Key, tasks=ress.OrderByDescending(a=>a.name.Length) };
            foreach(var user in result)
            {
                Console.WriteLine(user.Key);
                Console.WriteLine("List of tasks");
                foreach(var task in user.tasks)
                {
                    Console.WriteLine("\t"+ task);
                }
                Console.WriteLine("===========================================================");
            }
        }
        static async Task<List<Project>> GetAllProject(string path)
        {
            List<Project> projects = null;
            var response = await client.GetAsync(path);
            var jsonObject = await response.Content.ReadAsStringAsync();
            projects = JsonConvert.DeserializeObject<List<Project>>(jsonObject);
            return projects;
        }
        static async Task<List<Tasks>> GetAllTask(string path)
        {
            List<Tasks> projects = null;
            var response = await client.GetAsync(path);
            var jsonObject = await response.Content.ReadAsStringAsync();
            projects = JsonConvert.DeserializeObject<List<Tasks>>(jsonObject);
            return projects;
        }
        static async Task<List<Team>> GetAllTeam(string path)
        {
            List<Team> projects = null;
            var response = await client.GetAsync(path);
            var jsonObject = await response.Content.ReadAsStringAsync();
            projects = JsonConvert.DeserializeObject<List<Team>>(jsonObject);
            return projects;
        }
        static async Task<List<User>> GetAllUser(string path)
        {
            List<User> projects = null;
            var response = await client.GetAsync(path);
            var jsonObject = await response.Content.ReadAsStringAsync();
            projects = JsonConvert.DeserializeObject<List<User>>(jsonObject);
            return projects;
        }
        static async Task<List<TaskStateModel>> GetAllTaskState(string path)
        {
            List<TaskStateModel> projects = null;
            var response = await client.GetAsync(path);
            var jsonObject = await response.Content.ReadAsStringAsync();
            projects = JsonConvert.DeserializeObject<List<TaskStateModel>>(jsonObject);
            return projects;
        }
        static async Task MenuAsync()
        {
            int number=0;
            Console.WriteLine("Hello)\n Enter nmber");
            Console.WriteLine("\t1- Request 1\n" +
                "\t2- Request 2\n" +
                "\t3- Request 3\n" +
                "\t4- Request 4\n" +
                "\t5- Request 5\n" +
                "\t6- Request 6\n" +
                "\t7- Request 7\n");
            number=Int32.Parse(Console.ReadLine());
            int enterNumber;
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    await GetCountOfTaskInProjectByUserId(enterNumber);
                    break;
                case 2:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    await GetTaskOfUser(enterNumber);
                    break;
                case 3:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    await GetFinishedTasks(enterNumber);
                    break;
                case 4:
                    await GetTeamWithYoungUser();
                    break;
                case 5:
                    await GetSortedListByUserAndTeam();
                    break;
                case 6:
                    Console.WriteLine("Enter user Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    await GetByUserId(enterNumber);
                    break;
                case 7:
                    Console.WriteLine("Enter project Id");
                    enterNumber = Int32.Parse(Console.ReadLine());
                    await GetByProjectId(enterNumber);
                    break;
            }
        }
        static async Task Run()
        {
            client.BaseAddress = new Uri("https://bsa2019.azurewebsites.net/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            await MenuAsync();
        }

        static void Main(string[] args)
        {
            Run().GetAwaiter().GetResult(); ;
        }
    }
}
