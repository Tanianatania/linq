﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class Tasks
    {
        public int id;
        public string name;
        public string description;
        public DateTime created_at;
        public DateTime finished_at;
        public int state;
        public int project_id;
        public int performer_id;

        public override string ToString()
        {
            return $"Id: {id}, Name: {name}, Description: {description.Replace("\n"," ")}, Create at: {created_at}, Finished at: {finished_at}" +
                $" State: {state}, Project Id: {project_id}, Perfomer Id: {performer_id}\n";
        }
    }
}
