﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    public class User
    {
        public int id;
        public string first_name;
        public string last_name;
        public string email;
        public DateTime birthday;
        public DateTime registered_at;
        public int? team_id;

        public override string ToString()
        {
            return $" First name: {first_name}, Last name: {last_name}, Email: {email}, Birthday: {birthday}," +
                $"Registered at: {registered_at}, Team id: {team_id} ";
        }
    }
}
